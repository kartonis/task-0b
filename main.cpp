#include <iostream>
#include <map>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;

bool compare(const pair<string, int> &first, const pair<string, int> &second)
{
    return second.second<first.second;
}

void split(string &s,map<string, int> &Map, int &amount)
{
   // cout << s << endl;
    string new_s = "\0";
    for(int i=0; i<s.size()+1; ++i)
    {
        if(s[i]==' ' || s[i] == '\0')
        {
            amount++;
            pair< map< string, int >::iterator, bool > res;
            res = Map.insert(pair<string, int> (new_s,1));
            if (!res.second) res.first->second++;
           // cout << new_s << endl;
            new_s.erase(new_s.begin(), new_s.end());
        }
        else new_s += s[i];
    }
}
int main(int argc, const char * argv[]) {
    if(argc!=3)
        return 1;
    ifstream infile (argv[1]);
    ofstream outfile (argv[2]);
    string s;
    int amount=0;
    map <string, int> Map;
    while(!infile.eof())
    {
        getline(infile,s);
        split(s, Map, amount);
    }
    vector< pair< string, int > > vec(Map.begin(), Map.end());
    sort(vec.begin(), vec.end(), compare);
    outfile << "Word,Frequancy,Frequancy(%)\n";
    for(int i=0; i < vec.size(); ++i)
        outfile << vec[i].first << ',' << vec[i].second << ',' << (vec[i].second*100.0)/amount << '\n';
    infile.close();
    outfile.close();
    return 0;
}


